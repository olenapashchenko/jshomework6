/*
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
Екранування в мовах програмування необхідне для того, щоб можна було використовувати в коді спеціальні 
символи як звичайні.
2. Які засоби оголошення функцій ви знаєте?
Такі засоби оголошення функції, як: Function Declaration, Function Expression, Named Function Expression.
3. Що таке hoisting, як він працює для змінних та функцій?
Hoisting, або підняття - це процес доступу до змінних до їхнього визначення - відбувається
підняття коду вгору з визначенням змінних і функцій  перед їхнім використанням.
*/


function createNewUser() {
    let userName = prompt("Enter your name");
    let userLastName = prompt("Enter your last name");
    let userDateBirth = prompt("Enter your date of birth, 'dd.mm.yyyy'");

    const newUser = {
    firstName: userName,
    lastName: userLastName,
    birthday: userDateBirth,
    getLogin() {
        return (this.firstName[0] + this.lastName).toLowerCase(); 
    } ,
    getAge() {
        let birthYear = new Date(Date.parse(userDateBirth.split('.').reverse().join())) .getFullYear();
        let date = new Date(Date.now()).getFullYear();
        this.birthday = date - birthYear;
        return this.birthday;
    } ,
    getPassword() {
        return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday);
    } ,
} 
return newUser
}

const user = createNewUser()

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());